Feature: Crawling Sainsbury's website for products

  Scenario: Display error if website to crawl for products is down
    Given The server is mocked with a status of 500
    When I call to get a list of products
    Then the products list is empty
    And an error has been reported with message "Http error occurred [status=500] - HTTP error fetching URL"

  Scenario: Display error if page to crawl for products does not exist
    Given The server is mocked with a status of 404
    When I call to get a list of products
    Then the products list is empty
    And an error has been reported with message "Http error occurred [status=404] - HTTP error fetching URL"

  Scenario: Display a list of products without calories and description if details link is broken
    Given The server is mocked to return info from main page but not details page
    When I call to get a list of products
    Then a json list is returned
    And the list contains 1 products
    And the total gross is 1.75 and vat is 0.29
    And the data returned contains the following products
      | title                         | unitPrice | description                 | calories |
      | Sainsbury's Strawberries 400g | 1.75      | null | null       |

  Scenario: Display a list of products in json format where site contains just 1 product
    Given The server is mocked for a single product response with a status of 200
    When I call to get a list of products
    Then a json list is returned
    And the list contains 1 products
    And the total gross is 1.75 and vat is 0.29
    And the data returned contains the following products
      | title                         | unitPrice | description                 | calories |
      | Sainsbury's Strawberries 400g | 1.75      | by Sainsbury's strawberries | 33       |

  Scenario: Display a list of products in json format where site contains just 2 products
    Given The server is mocked for 2 products response with a status of 200
    When I call to get a list of products
    Then a json list is returned
    And the list contains 2 products
    And the total gross is 3.50 and vat is 0.58
    And the data returned contains the following products
      | title                         | unitPrice | description                 | calories |
      | Sainsbury's Strawberries 400g | 1.75      | by Sainsbury's strawberries | 33       |
      | Sainsbury's Blueberries 200g  | 1.75      | by Sainsbury's blueberries  | 45       |

  Scenario: Display a list of products in json format and excludes cross sell items,
    Given The server is mocked for cross sell products response with a status of 200
    When I call to get a list of products
    Then a json list is returned
    And the list contains 5 products
    And the total gross is 10.75 and vat is 1.79
    And the data returned contains the following products
      | title                                               | unitPrice | description                 | calories |
      | Sainsbury's Strawberries 400g                       | 1.75      | by Sainsbury's strawberries | 33       |
      | Sainsbury's Blueberries 200g                        | 1.75      | by Sainsbury's blueberries  | 45       |
      | Sainsbury's Blueberries 400g                        | 3.25      | by Sainsbury's blueberries  | 45       |
      | Sainsbury's Strawberries, Taste the Difference 300g | 2.5       | Ttd strawberries            | 33       |
      | Sainsbury's Cherry Punnet 200g                      | 1.5       | Cherries                    | 52       |

  Scenario: Should not show calories if not present on the website
    Given The server is mocked for cross sell products with no calories response with a status of 200
    When I call to get a list of products
    Then a json list is returned
    And the list contains 1 products
    And the total gross is 4.0 and vat is 0.67
    And the data returned contains the following products
      | title                                             | unitPrice | description                            | calories |
      | Sainsbury's British Cherry & Strawberry Pack 600g | 4         | British Cherry & Strawberry Mixed Pack | null     |

  Scenario: Display a list of products in json format and trim where description is multiple lines
    Given The server is mocked for products with long description 200
    When I call to get a list of products
    Then a json list is returned
    And the list contains 2 products
    And the total gross is 3.25 and vat is 0.54
    And the data returned contains the following products
      | title                          | unitPrice | description                 | calories |
      | Sainsbury's Strawberries 400g  | 1.75      | by Sainsbury's strawberries | 33       |
      | Sainsbury's Cherry Punnet 200g | 1.5       | Cherries                    | 52       |

  Scenario: Display a single product with description but with no nutritional stats
    Given The server is mocked for products with description but no nutrition info 200
    When I call to get a list of products
    Then a json list is returned
    And the list contains 1 products
    And the total gross is 1.75 and vat is 0.29
    And the data returned contains the following products
      | title                          | unitPrice | description                 | calories |
      | Sainsbury's Blackcurrants 150g  | 1.75      | Union Flag| null       |