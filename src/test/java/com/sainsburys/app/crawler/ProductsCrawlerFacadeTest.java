package com.sainsburys.app.crawler;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sainsburys.app.crawler.domain.Product;
import com.sainsburys.app.crawler.domain.ProductsReport;
import com.sainsburys.app.crawler.domain.Total;
import com.sainsburys.app.crawler.exceptions.CrawlingException;
import com.sainsburys.app.crawler.formatters.JsonFormatStrategy;
import com.sainsburys.app.crawler.service.ProductCrawlerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Asif Akhtar
 * 17/04/2020 00:28
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductsCrawlerFacadeTest {
    @InjectMocks
    private ProductsCrawlerFacade productsCrawlerFacade;

    @Mock
    private ProductCrawlerService productCrawlerService;

    @Test
    public void should_return_product_summary_in_json_format() {
        when(productCrawlerService.crawlForProducts(anyString())).thenReturn(stubProductData());
        String extractedJson = productsCrawlerFacade.crawl(Mockito.anyString(), new JsonFormatStrategy());
        assertThat(extractedJson, notNullValue());
        JsonObject jsonObject = JsonParser.parseString(extractedJson).getAsJsonObject();
        assertThat(jsonObject.getAsJsonArray("results"), notNullValue());
        assertThat(jsonObject.getAsJsonArray("results").size(), is(2));
        JsonObject total = jsonObject.getAsJsonObject("total");
        assertThat(total, notNullValue());
        assertThat(total.get("gross").getAsDouble(), is(0.0));
        verify(productCrawlerService).crawlForProducts(anyString());
    }

    @Test
    public void should_return_product_summary_as_domain() {
        when(productCrawlerService.crawlForProducts(anyString())).thenReturn(stubProductData());
        ProductsReport extractedData = productsCrawlerFacade.crawl(Mockito.anyString(), content -> stubProductData());
        assertThat(extractedData, notNullValue());
        assertThat(extractedData.getProducts(), notNullValue());
        assertThat(extractedData.getProducts().size(), is(2));
        assertThat(extractedData.getTotal(), notNullValue());
        assertThat(extractedData.getTotal().getGross(), is(0.0));
        verify(productCrawlerService).crawlForProducts(anyString());
    }

    @Test(expected = CrawlingException.class)
    public void crawler_should_propagate_exceptions() {
        when(productCrawlerService.crawlForProducts(anyString())).thenThrow(new CrawlingException("error"));
        try {
            productsCrawlerFacade.crawl(anyString(), new JsonFormatStrategy());
        } catch (CrawlingException e) {
            verify(productCrawlerService).crawlForProducts(anyString());
            throw e;
        }
    }

    private ProductsReport stubProductData() {
        ProductsReport productsReport = new ProductsReport();
        productsReport.setTotal(new Total());
        productsReport.setProducts(Arrays.asList(
                createProduct("Product1", 3.50, 100),
                createProduct("Product2", 5.45, 109)
        ));
        return productsReport;
    }

    private Product createProduct(String title, double price, int calories) {
        Product product = new Product();
        product.setTitle(title);
        product.setDescription(title);
        product.setCaloriesPer100g(calories);
        product.setUnitPrice(price);
        return product;
    }
}