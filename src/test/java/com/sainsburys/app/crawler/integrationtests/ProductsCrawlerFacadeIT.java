package com.sainsburys.app.crawler.integrationtests;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.gson.GsonBuilder;
import com.sainsburys.app.crawler.ProductsCrawlerFacade;
import com.sainsburys.app.crawler.domain.ProductsReport;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Asif Akhtar
 * 17/04/2020 14:35
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProductsCrawlerFacadeIT {
    @Rule
    public final WireMockRule rule = new WireMockRule(wireMockConfig().withRootDirectory("src/test/resources/stubbings").port(4556));
    @Autowired
    private ProductsCrawlerFacade productsCrawlerFacade;
    @Value("${crawler.wiremock.base.url}")
    private String wiremockBaseUrl;

    @Test
    public void should_crawl_website_for_products() {
        String serverUrl = wiremockBaseUrl + "/sainsburys.html";
        stubFor(get(urlPathMatching("/sainsburys.html"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile("sainsburys.html")));

        stubFor(get("/shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html")
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile("sainsburys-details.html")));
        String crawlProducts = productsCrawlerFacade.crawl(serverUrl);
        assertThat(crawlProducts, notNullValue());
        assertThat(crawlProducts.length() > 0, is(true));

        ProductsReport crawledProducts = new GsonBuilder().create().fromJson(crawlProducts, ProductsReport.class);
        assertThat(crawledProducts, notNullValue());
        assertThat(crawledProducts.getProducts().size(), is(17));

        verify(getRequestedFor(urlMatching("/sainsburys.html")));
    }
}
