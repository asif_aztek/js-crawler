package com.sainsburys.app.crawler.integrationtests;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.sainsburys.app.crawler.domain.ProductProperties;
import com.sainsburys.app.crawler.exceptions.CrawlingException;
import com.sainsburys.app.crawler.web.WebProductsCrawler;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.sainsburys.app.crawler.domain.Product.DESCRIPTION;
import static com.sainsburys.app.crawler.domain.Product.TITLE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Asif Akhtar
 * 17/04/2020 21:13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class WebProductsCrawlerIT {
    @Rule
    public final WireMockRule rule = new WireMockRule(wireMockConfig().withRootDirectory("src/test/resources/stubbings").port(4556));
    @Autowired
    private WebProductsCrawler webProductsCrawler;
    @Value("${crawler.wiremock.base.url}")
    private String wiremockBaseUrl;

    @Test
    public void should_connect_to_server_and_return_empty_properties_for_invalid_html() {
        String serverUrl = wiremockBaseUrl + "/sainsburys.html";
        stubFor(get(urlPathMatching("/sainsburys.html"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBody("<html> </html>")));

        List<ProductProperties> productProperties = webProductsCrawler.crawlForProducts(serverUrl);

        assertThat(productProperties.isEmpty(), is(true));
        verify(getRequestedFor(urlMatching("/sainsburys.html")));
    }

    @Test
    public void should_connect_to_server_and_return_product_properties_if_no_details_page() {
        String serverUrl = wiremockBaseUrl + "/sainsburys.html";
        stubFor(get(urlPathMatching("/sainsburys.html"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile("sainsburys.html")));

        stubFor(get("/shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html")
                .willReturn(aResponse()
                        .withStatus(404)));

        List<ProductProperties> productProperties = webProductsCrawler.crawlForProducts(serverUrl);

        assertThat(productProperties.isEmpty(), is(false));
        productProperties.forEach(prop -> assertThat(prop.count(), is(2)));
        assertProperty(productProperties.get(0), TITLE, "Sainsbury's Strawberries 400g");
        verify(getRequestedFor(urlMatching("/sainsburys.html")));
    }

    @Test
    public void should_connect_to_server_and_return_product_properties() {
        String serverUrl = wiremockBaseUrl + "/sainsburys.html";
        stubFor(get(urlPathMatching("/sainsburys.html"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile("sainsburys.html")));

        stubFor(get("/shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html")
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile("sainsburys-details.html")));
        List<ProductProperties> productProperties = webProductsCrawler.crawlForProducts(serverUrl);

        assertThat(productProperties.isEmpty(), is(false));
        productProperties.forEach(prop -> assertThat(prop.count(), is(4)));
        assertProperty(productProperties.get(0), TITLE, "Sainsbury's Strawberries 400g");
        assertProperty(productProperties.get(0), DESCRIPTION, "by Sainsbury's blueberries");

        verify(getRequestedFor(urlMatching("/sainsburys.html")));
    }

    @Test
    public void should_connect_to_server_and_return_properties_containing_multiple_line_desc() {
        String serverUrl = wiremockBaseUrl + "/sainsburys.html";
        stubFor(get(urlPathMatching("/sainsburys.html"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile("sainsburys.html")));

        stubFor(get("/shop/gb/groceries/berries-cherries-currants/sainsburys-blackcurrants-150g.html")
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile("sainsburys-details-desc.html")));
        List<ProductProperties> productProperties = webProductsCrawler.crawlForProducts(serverUrl);

        assertThat(productProperties.isEmpty(), is(false));
        productProperties.forEach(prop -> assertThat(prop.count(), is(4)));
        assertProperty(productProperties.get(5), TITLE, "Sainsbury's Blueberries, SO Organic 150g");
        assertProperty(productProperties.get(5), DESCRIPTION, "So Organic blueberries");

        verify(getRequestedFor(urlMatching("/sainsburys.html")));
    }

    @Test(expected = CrawlingException.class)
    public void should_throw_exception_when_server_returns_404() {
        String url404 = wiremockBaseUrl + "/404";
        stubFor(get(urlPathMatching("/404"))
                .willReturn(aResponse()
                        .withStatus(404)));
        try {
            webProductsCrawler.crawlForProducts(url404);
        } catch (CrawlingException e) {
            verify(getRequestedFor(urlMatching("/404")));
            throw e;
        }
    }

    private void assertProperty(ProductProperties property, String key, String expectedValue) {
        Optional<String> titleProperty = property.getProperty(key);
        assertThat(titleProperty.isPresent(), is(true));
        titleProperty.ifPresent(prop -> assertThat(prop, is(expectedValue)));
    }
}
