package com.sainsburys.app.crawler.behaviourtests;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sainsburys.app.crawler.CrawlerTestApplication;
import com.sainsburys.app.crawler.ProductsCrawlerFacade;
import com.sainsburys.app.crawler.formatters.JsonFormatStrategy;
import com.sainsburys.app.crawler.shared.CalculateService;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.sainsburys.app.crawler.behaviourtests.WiremockStubbings.stubResponseWithFile;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author Asif Akhtar
 * 17/04/2020 09:56
 */
@SpringBootTest(classes = CrawlerTestApplication.class)
@ActiveProfiles("test")
public class CrawlingProductsSteps {
    private final Logger log = LoggerFactory.getLogger(CrawlingProductsSteps.class);
    @Autowired
    private ProductsCrawlerFacade productsCrawlerFacade;
    @Autowired
    private CalculateService calculateService;
    private WireMockServer wireMockServer = new WireMockServer(wireMockConfig().withRootDirectory("src/test/resources/stubbings/cucumber").port(4556));

    @Value("${crawler.wiremock.base.url}")
    private String wiremockBaseUrl;
    private String jsonResponse;
    private JsonObject jsonObject;
    private Exception exception;

    private void startWireMock() {
        wireMockServer.start();
        configureFor("localhost", 4556);
    }

    @Then("^The server is mocked for a single product response with a status of (\\d+)$")
    public void server_is_mocked_for_a_single_response(final int status) {
        startWireMock();
        stubResponseWithFile("/sainsburys.html", "bd_sainsburys_single.html", status);
        stubResponseWithFile("/shop/gb/groceries/berries-cherries-currants/.*", "sainsburys-british-strawberries-400g.html", status);
    }

    @Then("^The server is mocked to return info from main page but not details page$")
    public void server_is_mocked_for_a_single_response_but_no_details_response() {
        startWireMock();
        stubResponseWithFile("/sainsburys.html", "bd_sainsburys_single.html", 200);
        stubResponseWithFile("/shop/gb/groceries/berries-cherries-currants/.*", "sainsburys-british-strawberries-400g.html", 404);
    }

    @Then("^The server is mocked with a status of (\\d+)$")
    public void server_is_mocked_with_status_of(final int status) {
        startWireMock();
        stubFor(get(urlPathMatching("/sainsburys.html"))
                .willReturn(aResponse()
                        .withStatus(status)
                        .withHeader("Content-Type", "text/html")));
    }

    @Then("^The server is mocked for 2 products response with a status of (\\d+)$")
    public void server_is_mocked_for_2_products_response(final int status) {
        startWireMock();

        stubResponseWithFile("/sainsburys.html", "bd_sainsburys_2.html", status);
        stubResponseWithFile("sainsburys-british-strawberries-400g.html", status);
        stubResponseWithFile("sainsburys-blueberries-200g.html", status);
    }

    @Then("^The server is mocked for cross sell products response with a status of (\\d+)$")
    public void server_is_mocked_for_cross_sell_products_response(final int status) {
        startWireMock();

        stubResponseWithFile("/sainsburys.html", "bd_sainsburys_cross_sell.html", status);
        stubResponseWithFile("sainsburys-british-strawberries-400g.html", status);
        stubResponseWithFile("sainsburys-blueberries-200g.html", status);
        stubResponseWithFile("sainsburys-blueberries-400g.html", status);
        stubResponseWithFile("sainsburys-strawberries--taste-the-difference-300g.html", status);
        stubResponseWithFile("sainsburys-cherry-punnet-200g-468015-p-44.html", status);
    }

    @Then("^The server is mocked for products with long description (\\d+)$")
    public void server_is_mocked_for_products_with_long_description(final int status) {
        startWireMock();

        stubResponseWithFile("/sainsburys.html", "bd_sainsburys_long_desc.html", status);
        stubResponseWithFile("sainsburys-british-strawberries-400g.html", status);
        stubResponseWithFile("sainsburys-cherry-punnet-200g-468015-p-44.html", status);
    }

    @Then("^The server is mocked for products with description but no nutrition info (\\d+)$")
    public void server_is_mocked_for_products_with_description_but_no_nut_info(final int status) {
        startWireMock();

        stubResponseWithFile("/sainsburys.html", "bd_sainsburys_desc_no_nutrition.html", status);
        stubResponseWithFile("sainsburys-blackcurrants-150g.html", status);
    }

    @Then("^The server is mocked for cross sell products with no calories response with a status of (\\d+)$")
    public void server_is_mocked_for_products_with_no_calories_response(final int status) {
        startWireMock();

        stubResponseWithFile("/sainsburys.html", "bd_sainsburys_no_calories.html", status);
        stubResponseWithFile("sainsburys-british-cherry---strawberry-pack-600g.html", status);
    }

    @Given("^I call to get a list of products$")
    public void I_call_to_get_a_list_of_products() {
        String serverUrl = wiremockBaseUrl + "/sainsburys.html";
        try {
            jsonResponse = productsCrawlerFacade.crawl(serverUrl, new JsonFormatStrategy());
        } catch (Exception e) {
            this.exception = e;
            exception.printStackTrace();
        } finally {
            wireMockServer.stop();
        }
    }

    @Then("^a (json|xml) list is returned$")
    public void a_list_is_returned(final String returnType) {
        try {
            if (returnType.equalsIgnoreCase("json")) {
                jsonObject = JsonParser.parseString(jsonResponse).getAsJsonObject();
                assertThat(jsonObject, notNullValue());
                assertThat(jsonObject.getAsJsonArray("results"), notNullValue());
            } else {
                fail("type must be json|xml");
            }
        } catch (Exception exception) {
            this.exception = exception;
            exception.printStackTrace();
            fail();
        }
    }

    @Then("^the products list is empty")
    public void products_list_is_empty() {
        assertThat(jsonResponse, nullValue());
    }

    @Then("^an error has been reported with message \"(.+)\"")
    public void an_error_has_been_reported(String errorMessage) {
        assertThat(exception, notNullValue());
        assertThat(exception.getLocalizedMessage(), is(errorMessage));
    }

    @Then("^the list contains (\\d+) products$")
    public void the_list_contain_products(final int quantity) {
        assertThat(jsonObject.getAsJsonArray("results").size(), is(quantity));
    }

    @And("^the total gross is (.+) and vat is (.+)$")
    public void gross_total_and_vat(final double gross, final double vat) {
        JsonObject total = jsonObject.getAsJsonObject("total");
        assertThat(total, notNullValue());
        assertThat(total.get("gross").getAsDouble(), is(gross));
        assertThat(total.get("vat").getAsDouble(), is(vat));
    }

    @And("the data returned contains the following products")
    public void somePhrase(DataTable table) {
        List<Map<String, String>> list = table.asMaps(String.class, String.class);

        JsonArray productsArray = jsonObject.getAsJsonArray("results");

        for (int i = 0; i < list.size(); i++) {
            String title = list.get(i).get("title");
            String unitPrice = list.get(i).get("unitPrice");
            String description = list.get(i).get("description");
            String calories = list.get(i).get("calories");

            JsonObject jsonObject = JsonParser.parseString(productsArray.get(i).toString()).getAsJsonObject();
            assertThat(jsonObject.get("title").getAsString(), is(title));
            assertThat(calculateService.parseDouble(jsonObject.get("unitPrice").getAsString()), is(calculateService.parseDouble(unitPrice)));

            assertProperty(jsonObject, "description", description);
            assertProperty(jsonObject, "caloriesPer100g", calories);
        }
    }

    private void assertProperty(JsonObject jsonObject, String property, String expectedValue) {
        if (expectedValue.equalsIgnoreCase("null")) {
            assertThat(jsonObject.has(property), is(false));
        } else {
            assertThat(jsonObject.get(property).getAsString(), is(expectedValue));
        }
    }
}
