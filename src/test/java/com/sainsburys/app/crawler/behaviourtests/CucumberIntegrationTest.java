package com.sainsburys.app.crawler.behaviourtests;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * @author Asif Akhtar
 * 17/04/2020 09:55
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
        plugin = {"pretty", "html:target/cucumber"})
public class CucumberIntegrationTest {
}
