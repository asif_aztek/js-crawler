package com.sainsburys.app.crawler.behaviourtests;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

/**
 * @author Asif Akhtar
 * 19/04/2020 15:48
 */
public class WiremockStubbings {
    static void stubResponseWithFile(String url, String file, int status) {
        stubFor(get(urlPathMatching(url))
                .willReturn(aResponse()
                        .withStatus(status)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile(file))).setPriority(1);
    }

    static void stubResponseWithFile(String file, int status) {
        stubFor(get(urlPathMatching("/shop/gb/groceries/berries-cherries-currants/" + file))
                .willReturn(aResponse()
                        .withStatus(status)
                        .withHeader("Content-Type", "text/html")
                        .withBodyFile(file))).setPriority(1);
    }
}
