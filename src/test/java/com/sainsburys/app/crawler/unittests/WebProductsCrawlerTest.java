package com.sainsburys.app.crawler.unittests;

import com.sainsburys.app.crawler.domain.ProductProperties;
import com.sainsburys.app.crawler.exceptions.CrawlingException;
import com.sainsburys.app.crawler.web.ContentDownloader;
import com.sainsburys.app.crawler.web.WebProductsCrawler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.sainsburys.app.crawler.domain.Product.TITLE;
import static com.sainsburys.app.crawler.domain.Product.UNIT_PRICE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Asif Akhtar
 * 17/04/2020 16:37
 */
@RunWith(MockitoJUnitRunner.class)
public class WebProductsCrawlerTest {
    @InjectMocks
    private WebProductsCrawler webCrawler;
    @Mock
    private ContentDownloader contentDownloader;

    @Before
    public void setup() {
        ReflectionTestUtils.setField(webCrawler, "rootElement", "rootElement");
    }

    @Test
    public void should_return_a_list_of_product_with_properties_from_initial_page() {
        Elements elements = mockBasicProductInfo();

        Document mockedDoc = mock(Document.class);
        when(mockedDoc.getElementsByClass(anyString())).thenReturn(elements);

        when(contentDownloader.downloadContent(anyString())).thenReturn(mockedDoc);

        List<ProductProperties> productProperties = webCrawler.crawlForProducts("http://sainsburys.com");
        productProperties.forEach(prop -> {
            assertThat(prop.count(), is(2));
            assertProperty(prop, TITLE, "ProductTitle");
            assertProperty(prop, UNIT_PRICE, "1.25");
        });

        assertThat(productProperties.size(), is(1));
        verify(contentDownloader).downloadContent(anyString());
        verifyNoMoreInteractions(contentDownloader);
    }

    @Test
    public void should_return_a_list_of_product_with_no_properties_from_initial_page() {
        Elements elements = mock(Elements.class);

        Document mockedDoc = mock(Document.class);
        when(mockedDoc.getElementsByClass(anyString())).thenReturn(elements);

        when(contentDownloader.downloadContent(anyString())).thenReturn(mockedDoc);

        List<ProductProperties> productProperties = webCrawler.crawlForProducts("http://sainsburys.com");
        productProperties.forEach(prop -> {
            assertThat(prop.count(), is(2));
            assertProperty(prop, TITLE, "ProductTitle");
            assertProperty(prop, UNIT_PRICE, "1.25");
        });

        assertThat(productProperties.size(), is(0));
        verify(contentDownloader).downloadContent(anyString());
        verifyNoMoreInteractions(contentDownloader);
    }

    @Test
    public void should_return_a_list_of_product_when_details_returns_404() {
        Elements elements = mockBasicProductInfo();

        Document mockedDoc = mock(Document.class);
        when(mockedDoc.getElementsByClass(anyString())).thenReturn(elements);

        when(contentDownloader.downloadContent(anyString())).thenReturn(mockedDoc);

        List<ProductProperties> productProperties = webCrawler.crawlForProducts("http://sainsburys.com");
        productProperties.forEach(prop -> {
            assertThat(prop.count(), is(2));
            assertProperty(prop, TITLE, "ProductTitle");
            assertProperty(prop, UNIT_PRICE, "1.25");
        });

        assertThat(productProperties.size(), is(1));
        verify(contentDownloader).downloadContent(anyString());
        verifyNoMoreInteractions(contentDownloader);
    }

    @Test
    public void should_return_a_list_of_product_with_properties_from_details_page() {
        Document mockedDoc = mock(Document.class);
        Elements mainElem = createElementWithProductDetails();
        when(mockedDoc.getElementsByClass(anyString())).thenReturn(mainElem);

        Document mockedDetailedDoc = mock(Document.class);
        Elements detailsPageElement = mockDetailsPageElement("This is ProductTitle info");
        Elements caloriesElement = mockCaloriesElement();
        when(mockedDetailedDoc.getElementsByClass(anyString())).thenReturn(detailsPageElement).thenReturn(caloriesElement);

        when(contentDownloader.downloadContent(anyString())).thenReturn(mockedDoc).thenReturn(mockedDetailedDoc);

        List<ProductProperties> productProperties = webCrawler.crawlForProducts("http://sainsburys.com");
        productProperties.forEach(prop -> {
            assertThat(prop.count(), is(4));
            assertProperty(prop, TITLE, "ProductTitle");
            assertProperty(prop, UNIT_PRICE, "1.25");
            assertProperty(prop, "description", "This is ProductTitle info");
            assertProperty(prop, "kcal_per_100g", "45");
        });

        assertThat(productProperties.size(), is(1));
        verify(contentDownloader, times(2)).downloadContent(anyString());
        verifyNoMoreInteractions(contentDownloader);
    }

    @Test
    public void should_return_a_list_of_product_with_properties_from_details_page_using_calories_table() {
        Document mockedDoc = mock(Document.class);
        Elements mainElem = createElementWithProductDetails();
        when(mockedDoc.getElementsByClass(anyString())).thenReturn(mainElem);

        Document mockedDetailedDoc = mock(Document.class);
        Elements detailsPageElement = mockDetailsPageElement("This is ProductTitle info");
        Elements caloriesElement = mockCaloriesElementUsingTable();
        when(mockedDetailedDoc.getElementsByClass(anyString())).thenReturn(detailsPageElement).thenReturn(null);
        when(mockedDetailedDoc.select(anyString())).thenReturn(caloriesElement);

        when(contentDownloader.downloadContent(anyString())).thenReturn(mockedDoc).thenReturn(mockedDetailedDoc);

        List<ProductProperties> productProperties = webCrawler.crawlForProducts("http://sainsburys.com");
        productProperties.forEach(prop -> {
            assertThat(prop.count(), is(4));
            assertProperty(prop, TITLE, "ProductTitle");
            assertProperty(prop, UNIT_PRICE, "1.25");
            assertProperty(prop, "description", "This is ProductTitle info");
            assertProperty(prop, "kcal_per_100g", "45");
        });

        assertThat(productProperties.size(), is(1));
        verify(contentDownloader, times(2)).downloadContent(anyString());
        verifyNoMoreInteractions(contentDownloader);
    }

    @Test
    public void should_return_a_list_of_product_without_calories_from_details_page() {
        Document mockedDoc = mock(Document.class);
        Elements mainElem = createElementWithProductDetails();
        when(mockedDoc.getElementsByClass(anyString())).thenReturn(mainElem);

        Document mockedDetailedDoc = mock(Document.class);
        Elements detailsElem1 = mockDetailsPageElement("This is ProductTitle info");
        when(mockedDetailedDoc.getElementsByClass(anyString())).thenReturn(detailsElem1).thenReturn(new Elements());

        when(contentDownloader.downloadContent(anyString())).thenReturn(mockedDoc).thenReturn(mockedDetailedDoc);

        List<ProductProperties> productProperties = webCrawler.crawlForProducts("http://sainsburys.com");
        productProperties.forEach(prop -> {
            assertThat(prop.count(), is(3));
            assertProperty(prop, TITLE, "ProductTitle");
            assertProperty(prop, UNIT_PRICE, "1.25");
            assertProperty(prop, "description", "This is ProductTitle info");
        });

        assertThat(productProperties.size(), is(1));
        verify(contentDownloader, times(2)).downloadContent(anyString());
        verifyNoMoreInteractions(contentDownloader);
    }

    @Test
    public void should_return_a_list_of_product_without_description_from_details_page() {
        Elements elements = createElementWithProductDetails();

        Document mockedDoc = mock(Document.class);
        when(mockedDoc.getElementsByClass(anyString())).thenReturn(elements);

        Document mockedDetailedDoc = mock(Document.class);

        when(contentDownloader.downloadContent(anyString())).thenReturn(mockedDoc).thenReturn(mockedDetailedDoc);

        List<ProductProperties> productProperties = webCrawler.crawlForProducts("http://sainsburys.com");
        productProperties.forEach(prop -> {
            assertThat(prop.count(), is(2));
            assertProperty(prop, TITLE, "ProductTitle");
            assertProperty(prop, UNIT_PRICE, "1.25");
        });

        assertThat(productProperties.size(), is(1));
        verify(contentDownloader, times(2)).downloadContent(anyString());
        verifyNoMoreInteractions(contentDownloader);
    }

    @Test(expected = CrawlingException.class)
    public void should_handle_when_error_is_thrown_by_html_downloader() {
        when(contentDownloader.downloadContent(anyString())).thenThrow(new CrawlingException("error"));

        try {
            webCrawler.crawlForProducts("http://sainsburys.com");
        } catch (CrawlingException e) {
            verify(contentDownloader).downloadContent(anyString());
            verifyNoMoreInteractions(contentDownloader);
            throw e;
        }
    }

    private void assertProperty(ProductProperties prop, String key, String expectedValue) {
        Optional<String> meta = prop.getProperty(key);
        assertThat(meta.isPresent(), is(true));
        meta.ifPresent(e -> assertThat(e, is(expectedValue)));
    }

    private Elements createElements() {
        Elements elements = new Elements();
        Element mockedElement = mock(Element.class);
        elements.add(mockedElement);
        return elements;
    }

    private Elements wrapElements(Elements innerElements) {
        Elements elements = createElements();
        when(elements.get(0).select(anyString())).thenReturn(innerElements);
        return elements;
    }

    private Elements mockBasicProductInfo() {
        Elements elements = createElements();
        when(elements.get(0).text()).thenReturn("1.25").thenReturn("ProductTitle");
        return wrapElements(elements);
    }

    private Elements mockDetailsPageElement(String description) {
        Elements elements = createElements();
        Node node = createNode(description);
        when(elements.get(0).childNodes()).thenReturn(Arrays.asList(node, node));
        return wrapElements(elements);
    }

    private Elements createElementWithProductDetails() {
        Elements innerElements = mockLinkElement();

        Elements elements = createElements();
        when(elements.get(0).select(anyString())).thenReturn(innerElements);
        return elements;
    }

    private Elements mockCaloriesElement() {
        Elements elements = createElements();
        Element mockedElement = mock(Element.class);
        elements.add(mockedElement);
        Node node = createNode("45cal");
        when(elements.get(0).childNodes()).thenReturn(Arrays.asList(node, node));
        return elements;
    }

    private Elements mockCaloriesElementUsingTable() {
        Elements elements = createElements();
        Element mockedElement = mock(Element.class);
        elements.add(mockedElement);
        Node node = createNode("45cal");
        when(elements.get(1).childNodes()).thenReturn(Collections.singletonList(node));
        return elements;
    }

    private Node createNode(String value) {
        Node node = mock(Node.class);
        when(node.childNodes()).thenReturn(Collections.singletonList(node));
        when(node.toString()).thenReturn(value);
        return node;
    }

    private Elements mockLinkElement() {
        Elements elements = createElements();
        when(elements.get(0).text()).thenReturn("1.25").thenReturn("ProductTitle");
        when(elements.get(0).attr(anyString())).thenReturn("http://detailspage");
        return elements;
    }
}