package com.sainsburys.app.crawler.unittests;


import com.sainsburys.app.crawler.domain.Product;
import com.sainsburys.app.crawler.domain.ProductProperties;
import com.sainsburys.app.crawler.domain.ProductsReport;
import com.sainsburys.app.crawler.exceptions.CrawlingException;
import com.sainsburys.app.crawler.service.ProductCrawlerService;
import com.sainsburys.app.crawler.shared.CalculateService;
import com.sainsburys.app.crawler.web.ProductsCrawler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static com.sainsburys.app.crawler.domain.Product.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author Asif Akhtar
 * 16/04/2020 22:32
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductsCrawlerServiceTest {
    @InjectMocks
    private ProductCrawlerService service;

    @Mock
    private ProductsCrawler productsCrawler;
    @Spy
    private final CalculateService calculateService = new CalculateService();

    @Before
    public void setup() {
        ReflectionTestUtils.setField(calculateService, "vatRate", 0.20);
    }

    @Test
    public void service_should_return_product_summary_for_single_product() {
        TestData testData = new TestData(TITLE, DESCRIPTION, 1.99f, 299);
        when(productsCrawler.crawlForProducts(anyString())).thenReturn(singletonList(createProductProperties(testData)));
        ProductsReport productsReport = service.crawlForProducts("http://sainsburys.com");
        assertThat(productsReport, notNullValue());
        assertTotal(productsReport, 1.99, 0.33);
        assertThat(productsReport.getProducts(), notNullValue());
        assertThat(productsReport.getProducts().size(), is(1));
        assertProduct(productsReport.getProducts().get(0), testData);
    }

    @Test
    public void service_should_return_product_summary_for_multiple_products() {
        TestData testData1 = new TestData(TITLE, DESCRIPTION, 1.99, 299);
        TestData testData2 = new TestData(TITLE + "1", DESCRIPTION + "1", 1.25, 299);
        when(productsCrawler.crawlForProducts(anyString())).thenReturn(asList(createProductProperties(testData1), createProductProperties(testData2)));
        ProductsReport productsReport = service.crawlForProducts("http://sainsburys.com");
        assertThat(productsReport, notNullValue());
        assertTotal(productsReport, 3.24, 0.54);
        assertThat(productsReport.getProducts(), notNullValue());
        assertThat(productsReport.getProducts().size(), is(2));
        assertProduct(productsReport.getProducts().get(0), testData1);
        assertProduct(productsReport.getProducts().get(1), testData2);
    }

    @Test(expected = CrawlingException.class)
    public void service_should_throw_exception_for_empty_url() {
        assertThat(service.crawlForProducts(""), notNullValue());
    }

    @Test(expected = CrawlingException.class)
    public void service_should_throw_exception_for_null_url() {
        assertThat(service.crawlForProducts(null), notNullValue());
    }

    @Test(expected = CrawlingException.class)
    public void service_should_throw_exception_for_invalid_url() {
        assertThat(service.crawlForProducts("htp://fake"), notNullValue());
    }

    private void assertTotal(ProductsReport productsReport, double gross, double vat) {
        assertThat(productsReport.getTotal(), notNullValue());
        assertThat(productsReport.getTotal().getGross(), is(gross));
        assertThat(productsReport.getTotal().getVat(), is(vat));
    }

    private void assertProduct(Product product, TestData expected) {
        assertThat(product.getCaloriesPer100g(), is(expected.calories));
        assertThat(product.getDescription(), is(expected.description));
        assertThat(product.getTitle(), is(expected.title));
        assertThat(product.getUnitPrice(), is(expected.unitPrice));
    }

    private ProductProperties createProductProperties(TestData testData) {
        ProductProperties properties = new ProductProperties();
        properties.add(TITLE, testData.title);
        properties.add(DESCRIPTION, testData.description);
        properties.add(UNIT_PRICE, "" + testData.unitPrice);
        properties.add(CALORIES, "" + testData.calories);
        return properties;
    }

    private static class TestData {
        String title;
        String description;
        double unitPrice;
        long calories;

        public TestData(String title, String description, double unitPrice, long calories) {
            this.title = title;
            this.description = description;
            this.unitPrice = unitPrice;
            this.calories = calories;
        }
    }
}