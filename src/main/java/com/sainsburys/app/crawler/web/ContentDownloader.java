package com.sainsburys.app.crawler.web;

import com.sainsburys.app.crawler.exceptions.CrawlingException;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author Asif Akhtar
 * 17/04/2020 22:19
 */
@Component
public class ContentDownloader {
    public Document downloadContent(String source) {
        try {
            return Jsoup.connect(source).timeout(6000).get();
        } catch (HttpStatusException e) {
            throw new CrawlingException(String.format("Http error occurred [status=%d] - %s", e.getStatusCode(), e.getLocalizedMessage()), e);
        } catch (IOException e) {
            throw new CrawlingException("Error occurred connecting to url - " + e.getLocalizedMessage(), e);
        } catch (Exception e) {
            throw new CrawlingException("unknown rror occurred connecting to url - " + e.getLocalizedMessage(), e);
        }
    }
}
