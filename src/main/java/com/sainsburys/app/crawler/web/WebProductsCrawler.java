package com.sainsburys.app.crawler.web;

import com.sainsburys.app.crawler.domain.ProductProperties;
import com.sainsburys.app.crawler.exceptions.CrawlingException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.sainsburys.app.crawler.domain.Product.*;

/**
 * @author Asif Akhtar
 * 17/04/2020 16:36
 */
@Component
public class WebProductsCrawler implements ProductsCrawler {
    private final Logger log = LoggerFactory.getLogger(WebProductsCrawler.class);
    private final ContentDownloader contentDownloader;

    @Value("${crawler.web.root.element}")
    private String rootElement;

    public WebProductsCrawler(ContentDownloader contentDownloader) {
        this.contentDownloader = contentDownloader;
    }

    @Override
    public List<ProductProperties> crawlForProducts(String source) {
        List<ProductProperties> productProperties = new ArrayList<>();
        Document doc = contentDownloader.downloadContent(source);
        Elements productElements = doc.getElementsByClass(rootElement);
        productElements.forEach(e -> {
            String absHref = extractLink(e);
            String price = extractPrice(e);
            String title = extractTitle(e);

            ProductProperties properties = new ProductProperties();
            properties.add(TITLE, title);
            properties.add(UNIT_PRICE, price);
            productProperties.add(properties);

            if (StringUtils.hasLength(absHref)) {
                try {
                    Document detailsDoc = contentDownloader.downloadContent(absHref);
                    extractDescription(properties, detailsDoc);
                    extractCalories(properties, detailsDoc);
                } catch (Exception er) {
                    log.error("Could not open details page - " + er.getLocalizedMessage(), er);
                }
            }
        });
        return productProperties;
    }

    private String extractLink(Element e) {
        Element link = e.select("*[class=\"productNameAndPromotions\"] a").first();
        return link.attr("abs:href");
    }

    private void extractCalories(ProductProperties properties, Document detailsDoc) {
        Elements caloriesElem = detailsDoc.getElementsByClass("tableRow0");
        Optional<String> calories = Optional.empty();
        if (Objects.nonNull(caloriesElem) && !caloriesElem.isEmpty()) {
            calories = traverseCalories(caloriesElem);
        } else {
            Elements elements = detailsDoc.select(".nutritionTable > tbody > tr>:nth-child(2)");
            if (Objects.nonNull(elements) && !elements.isEmpty()) {
                calories = traverseCaloriesNutritionalTable(elements);
            }
        }

        calories.ifPresent(cal -> properties.add(CALORIES, cal.replaceAll("[^\\d.]", "")));
    }

    private Optional<String> traverseCaloriesNutritionalTable(Elements elements) {
        try {
            return Optional.of(elements.get(1).childNodes().toString());
        } catch (Exception e) {
            log.error("Could not traverse doc for calories - " + e.getLocalizedMessage(), e);
            return Optional.empty();
        }
    }

    private Optional<String> traverseCalories(Elements caloriesElem) {
        try {
            return Optional.of(caloriesElem.get(0).childNodes().get(1).childNodes().get(0).toString());
        } catch (Exception e) {
            log.error("Could not traverse doc for calories - " + e.getLocalizedMessage(), e);
            return Optional.empty();
        }
    }

    private void extractDescription(ProductProperties properties, Document detailsDoc) {
        Elements descriptionElem = detailsDoc.getElementsByClass("mainProductInfo");
        if (Objects.nonNull(descriptionElem) && !descriptionElem.isEmpty()) {
            traverseDescription(descriptionElem).ifPresent(desc -> {
                properties.add(DESCRIPTION, Jsoup.parse(desc).text());
            });
        }
    }

    private Optional<String> traverseDescription(Elements descriptionElem) {
        try {
            return findDescription(descriptionElem.get(0).select("*[class*=\"productText\"] p"), 0);
        } catch (Exception e) {
            throw new CrawlingException("Could not traverse doc for description - " + e.getLocalizedMessage(), e);
        }
    }

    private Optional<String> findDescription(Elements elements, int index) {
        if (index <= elements.size()) {
            String description = elements.get(index).childNodes().get(0).toString();
            if (Objects.nonNull(description) && StringUtils.hasLength(description.trim())) {
                return Optional.of(description);
            } else {
                return findDescription(elements, ++index);
            }
        }
        return Optional.empty();
    }

    private String extractPrice(Element e) {
        try {
            return e.select("*[class=\"pricePerUnit\"]").get(0).text().replaceAll("[^\\d.]", "");
        } catch (Exception ex) {
            throw new CrawlingException("Could not traverse doc for price - " + ex.getLocalizedMessage(), ex);
        }
    }

    private String extractTitle(Element e) {
        try {
            return e.select("*[class=\"productNameAndPromotions\"]").get(0).text();
        } catch (Exception ex) {
            throw new CrawlingException("Could not traverse doc for title - " + ex.getLocalizedMessage(), ex);
        }
    }
}
