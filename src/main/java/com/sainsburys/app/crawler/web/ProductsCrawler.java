package com.sainsburys.app.crawler.web;

import com.sainsburys.app.crawler.domain.ProductProperties;

import java.util.List;

/**
 * @author Asif Akhtar
 * 17/04/2020 16:31
 */
public interface ProductsCrawler {
    List<ProductProperties> crawlForProducts(String source);
}
