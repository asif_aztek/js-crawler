package com.sainsburys.app.crawler.domain;

/**
 * @author Asif Akhtar
 * 16/04/2020 22:26
 */
public class Product {
    public static final String TITLE = "title";
    public static final String CALORIES = "kcal_per_100g";
    public static final String UNIT_PRICE = "unit_price";
    public static final String DESCRIPTION = "description";
    private String title;
    private Long caloriesPer100g;
    private Double unitPrice;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCaloriesPer100g() {
        return caloriesPer100g;
    }

    public void setCaloriesPer100g(long caloriesPer100g) {
        this.caloriesPer100g = caloriesPer100g;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override public String toString() {
        return "Product{" +
                "title='" + title + '\'' +
                ", caloriesPer100g=" + caloriesPer100g +
                ", unitPrice=" + unitPrice +
                ", description='" + description + '\'' +
                '}';
    }
}
