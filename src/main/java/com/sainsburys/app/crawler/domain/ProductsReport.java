package com.sainsburys.app.crawler.domain;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Asif Akhtar
 * 16/04/2020 22:29
 */
public class ProductsReport {
    @SerializedName(value = "results")
    private List<Product> products;
    private Total total;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Total getTotal() {
        return total;
    }

    public void setTotal(Total total) {
        this.total = total;
    }
}
