package com.sainsburys.app.crawler.domain;

/**
 * @author Asif Akhtar
 * 16/04/2020 22:30
 */
public class Total {
    private double gross;
    private double vat;

    public double getGross() {
        return gross;
    }

    public void setGross(double gross) {
        this.gross = gross;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }
}
