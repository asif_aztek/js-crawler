package com.sainsburys.app.crawler.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Asif Akhtar
 * 17/04/2020 16:32
 */
public class ProductProperties {
    private final Map<String, String> properties = new HashMap<>();

    public static ProductProperties of(String property, String value) {
        ProductProperties productProperty = new ProductProperties();
        productProperty.add(property, value);
        return productProperty;
    }

    public Optional<String> getProperty(String property) {
        return Optional.ofNullable(properties.get(property));
    }

    public void add(String property, String value) {
        properties.put(property, value);
    }

    public int count() {
        return properties.size();
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductProperties that = (ProductProperties) o;
        return Objects.equals(properties, that.properties);
    }

    @Override public int hashCode() {
        return Objects.hash(properties);
    }
}
