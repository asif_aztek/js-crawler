package com.sainsburys.app.crawler.service;

import com.sainsburys.app.crawler.domain.Product;
import com.sainsburys.app.crawler.domain.ProductProperties;
import com.sainsburys.app.crawler.domain.ProductsReport;
import com.sainsburys.app.crawler.domain.Total;
import com.sainsburys.app.crawler.exceptions.CrawlingException;
import com.sainsburys.app.crawler.shared.CalculateService;
import com.sainsburys.app.crawler.web.ProductsCrawler;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

import static com.sainsburys.app.crawler.domain.Product.*;

/**
 * @author Asif Akhtar
 * 16/04/2020 22:29
 */
@Service
public class ProductCrawlerService {
    private final ProductsCrawler productsCrawler;
    private final CalculateService calculateService;

    public ProductCrawlerService(ProductsCrawler productsCrawler, CalculateService calculateService) {
        this.productsCrawler = productsCrawler;
        this.calculateService = calculateService;
    }

    public ProductsReport crawlForProducts(String url) {
        validate(url);
        List<ProductProperties> productProperties = productsCrawler.crawlForProducts(url);

        ProductsReport productsReport = new ProductsReport();
        productsReport.setProducts(productProperties.stream().map(this::createProduct).collect(Collectors.toList()));
        calculateTotal(productsReport);

        return productsReport;
    }

    private void calculateTotal(ProductsReport productsReport) {
        double gross = calculateService.calculateGross(productsReport.getProducts());
        productsReport.setTotal(new Total());
        productsReport.getTotal().setGross(gross);
        productsReport.getTotal().setVat(calculateService.calculateVat(gross));
    }

    private Product createProduct(ProductProperties prop) {
        Product product = new Product();
        prop.getProperty(TITLE).ifPresent(product::setTitle);
        prop.getProperty(DESCRIPTION).ifPresent(product::setDescription);
        prop.getProperty(UNIT_PRICE).ifPresent(e -> product.setUnitPrice(calculateService.parseDouble(e)));
        prop.getProperty(CALORIES).ifPresent(e -> product.setCaloriesPer100g(calculateService.parseLong(e)));
        return product;
    }

    private void validate(String url) {
        if (StringUtils.isEmpty(url)) {
            throw new CrawlingException("url to crawl is empty");
        }
        if (!url.contains("http")) {
            throw new CrawlingException("url to crawl is invalid");
        }
    }
}
