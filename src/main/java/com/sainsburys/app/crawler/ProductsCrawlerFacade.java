package com.sainsburys.app.crawler;

import com.sainsburys.app.crawler.domain.ProductsReport;
import com.sainsburys.app.crawler.formatters.FormatStrategy;
import com.sainsburys.app.crawler.formatters.JsonFormatStrategy;
import com.sainsburys.app.crawler.service.ProductCrawlerService;
import org.springframework.stereotype.Component;

/**
 * @author Asif Akhtar
 * 17/04/2020 00:25
 */
@Component
public class ProductsCrawlerFacade {
    private final ProductCrawlerService productCrawlerService;

    public ProductsCrawlerFacade(ProductCrawlerService productCrawlerService) {
        this.productCrawlerService = productCrawlerService;
    }

    public <T> T crawl(String url, FormatStrategy<T> formatStrategy) {
        ProductsReport crawledProducts = productCrawlerService.crawlForProducts(url);
        return formatStrategy.format(crawledProducts);
    }

    public String crawl(String url) {
        return crawl(url, new JsonFormatStrategy());
    }

}
