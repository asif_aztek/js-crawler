package com.sainsburys.app.crawler.formatters;

import com.google.gson.GsonBuilder;

/**
 * @author Asif Akhtar
 * 16/04/2020 23:58
 */
public class JsonFormatStrategy implements FormatStrategy<String> {
    @Override public String format(Object content) {
        return new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .create()
                .toJson(content);
    }
}
