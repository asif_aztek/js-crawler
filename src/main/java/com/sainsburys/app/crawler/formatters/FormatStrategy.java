package com.sainsburys.app.crawler.formatters;

/**
 * @author Asif Akhtar
 * 16/04/2020 23:59
 */
@FunctionalInterface
public interface FormatStrategy<T> {
    T format(Object content);
}
