package com.sainsburys.app.crawler.formatters;

/**
 * @author Asif Akhtar
 * 18/04/2020 23:58
 */
public class StringFormatStrategy implements FormatStrategy<String> {
    @Override public String format(Object content) {
        return content.toString();
    }
}
