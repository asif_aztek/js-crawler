package com.sainsburys.app.crawler.exceptions;

/**
 * @author Asif Akhtar
 * 16/04/2020 22:56
 */
public class CrawlingException extends RuntimeException {

    public CrawlingException(String message) {
        super(message);
    }

    public CrawlingException(String message, Throwable cause) {
        super(message, cause);
    }
}
