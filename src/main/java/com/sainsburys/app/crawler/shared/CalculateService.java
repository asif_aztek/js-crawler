package com.sainsburys.app.crawler.shared;

import com.sainsburys.app.crawler.domain.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;

/**
 * @author Asif Akhtar
 * 19/04/2020 20:30
 */
@Service
public class CalculateService {
    @Value("${crawler.data.stats.vat}")
    private double vatRate;

    public double parseDouble(String amount) {
        return Double.parseDouble(amount);
    }

    public long parseLong(String amount) {
        return Long.parseLong(amount);
    }

    public double calculateVat(double gross) {
        if (gross < 1) {
            return gross;
        }
        double netAmount = gross / (1 + vatRate);
        return scale(gross - netAmount, 2);
    }

    public double calculateGross(List<Product> products) {
        return scale(products.stream().mapToDouble(Product::getUnitPrice).sum(), 2);
    }

    private double scale(double amount, int scale) {
        return valueOf(amount).setScale(scale, HALF_UP).doubleValue();
    }
}
