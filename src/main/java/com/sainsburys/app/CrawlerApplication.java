package com.sainsburys.app;

import com.sainsburys.app.crawler.ProductsCrawlerFacade;
import com.sainsburys.app.crawler.formatters.JsonFormatStrategy;
import com.sainsburys.app.crawler.formatters.StringFormatStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;

@SpringBootApplication
public class CrawlerApplication {
    private final ProductsCrawlerFacade productsCrawlerFacade;
    @Value("${crawler.web.app.url}")
    private String url;
    @Value("${crawler.web.app.strategy}")
    private String strategy;

    public CrawlerApplication(ProductsCrawlerFacade productsCrawlerFacade) {
        this.productsCrawlerFacade = productsCrawlerFacade;
    }

    public static void main(String[] args) {
        disableSpringBanner(args);
    }

    private static void disableSpringBanner(String[] args) {
        SpringApplication app = new SpringApplication(CrawlerApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.setAddCommandLineProperties(false);
        app.run(args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            System.out.println("Starting Command Line  Application");
//            for (String arg : args) {
//                System.out.println(arg);
//            }

            try {
                if (StringUtils.isEmpty(strategy) || strategy.equalsIgnoreCase("json")) {
                    System.out.println(productsCrawlerFacade.crawl(url, new JsonFormatStrategy()));
                } else {
                    System.out.println(productsCrawlerFacade.crawl(url, new StringFormatStrategy()));
                }
            } catch (Exception exception) {
                System.err.println("Failed to Run application - " + exception.getLocalizedMessage());
            }
        };
    }
}
