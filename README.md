# js-crawler

This is a solution to the "serverside-test" exercise

Author: **ASIF AKHTAR**


## Building Project


```bash
./gradlew clean build
```

## Notes
The cucumber and integration test start up a wiremock instance locally on **localhost:4556**

## Running Tests
The following will run unit, integration and cucumber tests

```bash
./gradlew cleanTest test
```

## Running Cucumber Tests
The following will run only the cucumber tests

```bash
./gradlew cucumber
```

## Usage
```bash
./gradlew bootRun
```
The application will download the default url configured in application.properties
```bash
crawler.web.app.url= #url of the website to crawl
crawler.web.app.strategy= #'json' or 'string' outputs as depending on value
```

